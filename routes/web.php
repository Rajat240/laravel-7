<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::group(["namespace" => "Admin" , "prefix" => "admin"], function(){

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Auth\LoginController@admin');
    Route::post('dashboard', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');
});

//});
Route::get('/', function () {
    return view('welcome');
});
